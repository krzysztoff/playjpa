package com.example.playjpa;

import com.google.common.collect.Sets;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.stereotype.Component;

import java.util.Arrays;

public class TestDataInit {


    public static Long loadUser(TestEntityManager testEntityManager) {
        OrderLine orderLine1 = testEntityManager.persist(new OrderLine("Product_1", 2));
        OrderLine orderLine2 = testEntityManager.persistAndFlush(new OrderLine("Product_2", 2));

        Orders order = testEntityManager.persistAndFlush(new Orders(Arrays.asList(orderLine1, orderLine2)));

//        Customer customer = testEntityManager.persist(new Customer(Arrays.asList(order)));

        Message lazyMessage = testEntityManager.persist(new Message("Leniwa"));
        Message eagerMessage = testEntityManager.persist(new Message("Zachlanna"));
        Message lazyMessageToList = testEntityManager.persist(new Message("Leniwa_z_listy"));


        Customer entity = new Customer(Sets.newHashSet(order), Sets.newHashSet(lazyMessage), Sets.newHashSet(eagerMessage), Lists.newArrayList(lazyMessageToList));
        return (Long) testEntityManager.persistAndGetId(entity);

    }
}
