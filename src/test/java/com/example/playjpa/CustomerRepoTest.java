package com.example.playjpa;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
//@SpringBootTest
@DataJpaTest(showSql = true)
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
public class CustomerRepoTest {

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private TestEntityManager testEntityManager;
    Long userId;

    @Before
    public void setUp() throws Exception {
        userId = TestDataInit.loadUser(testEntityManager);
    }

    @Test
    public void name() {


        Customer one = customerRepo.findByIdAndDontFetch(userId).get();
        System.out.println();
        testEntityManager.clear();
        testEntityManager.flush();
        Customer customer = customerRepo.findById(userId).get();
        System.out.println(customer.getLazyMessages());
    }

    @Test
    public void name2() {
        Customer one = customerRepo.getOne(4L);
        System.out.println();
    }
}
