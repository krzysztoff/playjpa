package com.example.playjpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderLine extends BaseEntity{

    private String productName;
    private Integer quantity;
}
