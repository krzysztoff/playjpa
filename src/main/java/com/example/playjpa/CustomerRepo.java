package com.example.playjpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CustomerRepo extends JpaRepository<Customer, Long> {


    @Query("select c from Customer c join c.orderList ol join ol.orderLines")
    Optional<Customer> findByIdAndDontFetch(Long aLong);
}
