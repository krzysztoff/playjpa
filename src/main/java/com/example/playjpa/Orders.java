package com.example.playjpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Table(name = "Orders")
@AllArgsConstructor
@NoArgsConstructor
public class Orders extends BaseEntity{

    @OneToMany
    @JoinColumn(name = "order_id")
    private List<OrderLine> orderLines;
}
