package com.example.playjpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Customer extends BaseEntity {

    @OneToMany
    @JoinColumn(name = "customer_id")
    private Set<Orders> orderList;

    @OneToMany
    @JoinColumn(name = "lazy_customer_id")
    private Set<Message> lazyMessages;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "eager_customer_id")
    private Set<Message> eagerMessages;

    @OneToMany
    @JoinColumn(name = "lazy_list_customer_id")
    private List<Message> lazyMessagesList;

}
