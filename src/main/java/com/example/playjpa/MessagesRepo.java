package com.example.playjpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MessagesRepo extends JpaRepository<Message,Long> {
}
