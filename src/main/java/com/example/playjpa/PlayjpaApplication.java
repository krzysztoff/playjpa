package com.example.playjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlayjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlayjpaApplication.class, args);
    }

}
