package com.example.playjpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Set;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    public Customer findCustomer(Long id) {
        return customerRepo.getOne(id);
    }

    public Set<Message> provideLazyMessagesWithoutTransaction(Long id) {
        return customerRepo.findById(id).map(Customer::getLazyMessages).orElse(Collections.emptySet());
    }

    @Transactional
    public Set<Message> provideLazyMessagesWithTransaction(Long id) {
        return customerRepo.findById(id).map(Customer::getLazyMessages).orElse(Collections.emptySet());
    }
}
